//
//  BrowseQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/1/11.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

private let BrowseTable    = "BrowseTables"

private let id             = Expression<String>("primaryId")
private let chapter        = Expression<Int>("chapter")
private let pageIndex      = Expression<Int>("pageIndex")
private let data           = Expression<String?>("content")
private let updateTime     = Expression<TimeInterval>("updateTime")

class BrowseQueue{
    private class func createTable() -> Table{
        let tables = Table(BrowseTable)
        try! dataBase.run(tables.create(ifNotExists: true, block: { (table) in
            //注意.autoincrement
            table.column(id,primaryKey: true)
            table.column(chapter)
            table.column(pageIndex)
            
            table.column(data)
            table.column(updateTime)
            
        }))
        return tables
    }
}
extension BrowseQueue{
    public class func insertData(primaryId :String,content : String,chap :Int,page :Int,time :TimeInterval,completion:@escaping ((_ success : Bool) -> Void)){
        let table = createTable()
        let insertdata = table.insert(id <- primaryId,data <- content,chapter <- chap,pageIndex <- page,updateTime <- time)
        do {
            try dataBase.run(insertdata)
            completion(true)
        } catch  {
            debugPrint(error)
            completion(false)
        }
    }
    public class func updateData(primaryId :String,content : String?,chap :Int,page :Int,time :TimeInterval,completion:@escaping ((_ success : Bool) -> Void)){
        let table = createTable().filter(primaryId == id)
        var update : Update? = nil
        if content == nil {
            update = table.update(id <- primaryId,chapter <- chap,updateTime <- time,pageIndex <- page)
        }else{
            update = table.update(id <- primaryId,data <- content,chapter <- chap,updateTime <- time,pageIndex <- page)
        }
        do {
            try dataBase.run(update!)
            completion(true)
        } catch  {
            debugPrint(error)
            completion(false)
        }
    }
    public class func deleteData(primaryId :String,completion:@escaping ((_ success : Bool) -> Void)){
        let table = createTable()
        do {
            let alice = table.filter(id == primaryId)
            try dataBase.run(alice.delete())
            completion(true)
        } catch {
            debugPrint(error)
            completion(false)
        }
    }
    
    
    public class func searchData(primaryId :String,needChapter :Bool,completion:@escaping ((_ model : GKBrowseModel?) -> Void)){
        let table = createTable()
        let alice = table.filter(id == primaryId)
        guard let datas : AnySequence<Row> = try? dataBase.prepare(alice) else {
            completion(nil)
            return
        }
        print(Date.init(timeIntervalSinceNow: 0))
        decodeData(listData: datas,needChapter: needChapter) { (listData) in
        
            completion(listData.first)
        }
    }
    public class func searchData(page : Int,size : Int = 20,completion:@escaping ((_ datas : [GKBrowseModel]) ->Void)){
        let table = createTable()
        let order = table.order(updateTime.desc).limit(size, offset: (page - 1) * size)
        
        guard let datas : AnySequence<Row> = try? dataBase.prepare(order) else {
            completion([])
            return
        }
        decodeData(listData: datas,needChapter: false,completion: completion)
    }
    public class func searchData(completion:@escaping ((_ datas : [GKBrowseModel]) ->Void)){
        let table = createTable()
        guard let datas : AnySequence<Row> = try? dataBase.prepare(table) else {
            completion([])
            return
        }
        decodeData(listData: datas,completion: completion)
    }
    private class func decodeData(listData : AnySequence<Row>,needChapter :Bool = true,completion:@escaping ((_ datas : [GKBrowseModel]) ->Void)){
        DispatchQueue.global().async {
            var content : [GKBrowseModel] = []
            listData.forEach { (objc) in
                if let model = decode(objc: objc, needChapter: needChapter){
                    content.append(model)
                }
            }
            DispatchQueue.main.async {
                completion(content)
            }
        }
    }
    private class func decode(objc : Row,needChapter :Bool) -> GKBrowseModel?{
        let json = JSON(parseJSON: objc[data] ?? "")
        let time = TimeInterval(objc[updateTime])
        let chap = Int(objc[chapter])
        let page = Int(objc[pageIndex])
        var newData = json;
        if needChapter == false {
            newData["chapterInfo"] = []
        }
        if let model : GKBrowseModel = GKBrowseModel.deserialize(from: newData.rawString()){
            model.updateTime = time
            model.chapter = chap
            model.pageIndex = page
            return model
        }
        return nil
    }
}
