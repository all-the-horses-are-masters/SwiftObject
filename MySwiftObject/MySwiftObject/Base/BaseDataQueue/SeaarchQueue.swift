//
//  SeaarchQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/11/26.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit

private let SearchTable    = "SearchTable"

private let id             = Expression<Int>("primaryId")
private let hotName        = Expression<String?>("hotName")
private let updateTime     = Expression<TimeInterval>("time")
private let createTime     = Expression<TimeInterval>("createTime")

class SeaarchQueue: NSObject {
    private class func createTable() -> Table{
        let tables = Table(SearchTable)
        try! dataBase.run(tables.create(ifNotExists: true, block: { (table) in
            //注意.autoincrement
            table.column(id,primaryKey: .autoincrement)
            table.column(hotName)
            table.column(updateTime)
            
        }))
        return tables
    }
    private class func columns(table :String,column :String) -> Bool{
        do {
            var columnDatas :[String] = []
            let s = try dataBase.prepare("PRAGMA table_info(" + table + ")" )
            for row in s { columnDatas.append(row[1]! as! String) }
            let list = columnDatas.filter { (item) -> Bool in
                return item == column
            }
            return list.count > 0
        }
        catch {
            return false
        }
    }
    private class func selectCount(keyword :String) ->Int{
        let table = createTable()
        let haveColumn = columns(table: SearchTable, column: "createTime")
        if !haveColumn {//表结构升级 添加一列createTime
            do {
                try dataBase.run(table.addColumn(createTime,defaultValue:0))
            } catch  {
                debugPrint(error)
            }
        }
        let alice = table.filter(hotName == keyword)
        do {
            let count = try dataBase.scalar(alice.count)
            debugPrint(count)
            return count
        } catch  {
            return 0
        }
    }
}
extension SeaarchQueue{
    public class func insertData(keyword :String,completion:@escaping ((_ success : Bool) -> Void)){
        let count = selectCount(keyword: keyword)
        if count == 0 {
            let table = createTable()
            let time : TimeInterval = NSDate ().timeIntervalSince1970
            let insertdata = table.insert(hotName <- keyword,updateTime <- time,createTime <- time)
            do {
                try dataBase.transaction {
                    try dataBase.run(insertdata)
                }
                completion(true)
            } catch  {
                debugPrint(error)
                completion(false)
            }
        }else{
            updateData(keyword: keyword, completion: completion)
        }
    }
    public class func updateData(keyword :String,completion:@escaping ((_ success : Bool) -> Void)){
        let count = selectCount(keyword: keyword)
        if count == 0 {
            insertData( keyword: keyword, completion: completion)
        }else{
            let table = createTable().filter(hotName == keyword)
            let time : TimeInterval = NSDate ().timeIntervalSince1970
            let update = table.update(hotName <- keyword,updateTime <- time)
            do {
                try dataBase.transaction {
                    try dataBase.run(update)
                }
                completion(true)
            } catch  {
                debugPrint(error)
                completion(false)
            }
        }
    }
    public class func deleteData(keyword :String,completion:@escaping ((_ success : Bool) -> Void)){
        let table = createTable()
        do {
            try dataBase.transaction {
                let alice = table.filter(hotName == keyword)
                try dataBase.run(alice.delete())
            }
            completion(true)
        } catch {
            debugPrint(error)
            completion(false)
        }
    }
    public class func deleteData(keywords :[String],completion:@escaping ((_ success : Bool) -> Void)){
        do {
            try dataBase.transaction {
                keywords.forEach { (title) in
                    let table = createTable()
                    do {
                        let alice = table.filter(hotName == title)
                        try dataBase.run(alice.delete())
                    } catch {
                        debugPrint(error)
                    }
                }
            }
            completion(true)
        } catch  {
            completion(false)
        }
    }
    public class func searchData(page : Int,size : Int = 20,completion:@escaping ((_ datas : [String]) ->Void)){
        let table = createTable()
        let order = table.order(updateTime.desc).limit(size, offset: (page - 1) * size)
        guard let datas : AnySequence<Row> = try? dataBase.prepare(order) else {
            completion([])
            return
        }
        decodeData(listData: datas,completion: completion)
    }
    public class func searchData(completion:@escaping ((_ datas : [String]) ->Void)){
        let table = createTable()
        guard let datas : AnySequence<Row> = try? dataBase.prepare(table) else {
            completion([])
            return
        }
        decodeData(listData: datas,completion: completion)
    }
    private class func decodeData(listData : AnySequence<Row>,completion:@escaping ((_ datas : [String]) ->Void)){
        var contentData : [String] = []
        listData.forEach { (objc) in
            let content :String = objc[hotName] ?? ""
            contentData.append(content)
        }
        completion(contentData)
    }
}
