//
//  BaseQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/11/25.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
@_exported import SQLite
//数据库单利
public  let dataBase : Connection = BaseQueue.manager.db
//表主键
private let id          = Expression<String>("primaryId")
//表模型数据
private let data        = Expression<String?>("data")


class BaseQueue: NSObject {
    //单利
    static let manager = BaseQueue()
    lazy var db: Connection = {
        let path = BaseQueue.path()
        var db = try? Connection(path)
        db?.busyTimeout = 5.0//设置线程安全
        return db!
    }()
    private class func path() -> String{
        let path = NSHomeDirectory() + "/Documents/Caches/Sqlite"
        let file = FileManager.default
        if file.fileExists(atPath: path) == false{
            do {
                try file.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                debugPrint("create path success")
            } catch  {
                debugPrint(error.localizedDescription)
            }
            
        }
        return path + "DataBase.sqlite"
    }
    public class func dropTable(_ name :String,completion:@escaping ((_ success : Bool) ->Void)){
        let exeStr = "drop table if exists \(name) "
        do {
            try dataBase.execute(exeStr)
            completion(true)
        }catch{
            completion(false)
        }
    }
}
extension BaseQueue{
    private class func createTable(_ name :String) -> Table{
        let tables = Table(name)
        try! dataBase.run(tables.create(ifNotExists: true, block: { (table) in
            //注意.autoincrement
            table.column(id,primaryKey: true)
            table.column(data)
        }))
        return tables
    }
    public class func selectCount(tableName :String,primaryId :String) ->Int{
        let table = createTable(tableName)
        let alice = table.filter(id == primaryId)
        do {
            let count = try dataBase.scalar(alice.count)
            debugPrint(count)
            return count
        } catch  {
            return 0
        }
    }
    public class func insertData(tableName :String,primaryId :String,content : String,completion:@escaping ((_ success : Bool) -> Void)){
        let count = selectCount(tableName: tableName, primaryId: primaryId)
        if count == 0 {
            let table = createTable(tableName)
            let insertdata = table.insert(id <- primaryId,data <- content)
            do {
                try dataBase.run(insertdata)
                completion(true)
            } catch  {
                debugPrint(error)
                completion(false)
            }
        }else{
            updateData(tableName: tableName, primaryId: primaryId, content: content, completion: completion)
        }
    }
    public class func updateData(tableName :String,primaryId :String,content : String,completion:@escaping ((_ success : Bool) -> Void)){
        let count = selectCount(tableName: tableName, primaryId: primaryId)
        if count == 0 {
            insertData(tableName: tableName, primaryId: primaryId, content: content, completion: completion)
        }else{
            let table = createTable(tableName).filter(primaryId == id)
            let update = table.update(id <- primaryId,data <- content)
            do {
                try dataBase.run(update)
                completion(true)
            } catch  {
                debugPrint(error)
                completion(false)
            }
        }
    }
    public class func deleteData(tableName :String,primaryId :String,completion:@escaping ((_ success : Bool) -> Void)){
        let table = createTable(tableName)
        do {
            let alice = table.filter(id == primaryId)
            try dataBase.run(alice.delete())
            completion(true)
        } catch {
            debugPrint(error)
            completion(false)
        }
    }
    public class func searchData(tableName :String,primaryId :String,completion:@escaping ((_ datas : JSON) -> Void)){
        let table = createTable(tableName)
        let alice = table.filter(id == primaryId)
        guard let datas : AnySequence<Row> = try? dataBase.prepare(alice) else {
            completion("")
            return
        }
        decodeData(listData: datas, completion: completion)
    }
    public class func searchData(tableName :String,page : Int,size : Int = 20,completion:@escaping ((_ datas : JSON) ->Void)){
        let table = createTable(tableName)
        let order = table.order(id.asc).limit(size, offset: (page - 1) * size)
        
        guard let datas : AnySequence<Row> = try? dataBase.prepare(order) else {
            completion(JSON(""))
            return
        }
        decodeData(listData: datas,completion: completion)
    }
    public class func searchData(tableName :String,completion:@escaping ((_ datas : JSON) ->Void)){
        let table = createTable(tableName)
        guard let datas : AnySequence<Row> = try? dataBase.prepare(table) else {
            completion(JSON(""))
            return
        }
        decodeData(listData: datas,completion: completion)
    }
    private class func decodeData(listData : AnySequence<Row>,completion:@escaping ((_ datas : JSON) ->Void)){
        DispatchQueue.global().async {
            var contentData : [JSON] = []
            listData.forEach { (objc) in
                let json = JSON(parseJSON: objc[data] ?? "")
                contentData.append(json)
            }
            let json = JSON(contentData)
            DispatchQueue.main.async {
                completion(json)
            }
        }
    }
}


