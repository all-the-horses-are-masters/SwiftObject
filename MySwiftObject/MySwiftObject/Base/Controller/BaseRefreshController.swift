//
//  BaseRefreshController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/30.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
@_exported import ATRefresh_Swift
@_exported import AudioToolbox
public let RefreshPageStart : Int = (1)
public let RefreshPageSize  : Int = (20)

private let refreshErrorData  : UIImage = UIImage(named: "icon_net_error") ?? UIImage()
private let refreshLoaderToast: String = "Data loading..."
private var refreshEmptyData  : UIImage = UIImage()
private var refreshErrorToast : String  = ""
private var refreshEmptyToast : String  = ""
class BaseRefreshController: BaseViewController {
    public lazy var refreshData: ATRefreshData = {
        let refresh = ATRefreshData()
        refresh.delegate = self
        refresh.dataSource = self
        return refresh
    }()
    private lazy var images: [UIImage] = {
        var images :[UIImage] = [];
        for i in 0...4{
            let image = UIImage(named:"loading\(i)");

            if image != nil {
                images.append(image!);
            }
        }
        for i in 0...4{
            let image = UIImage(named:"loading\(4-i)");

            if image != nil {
                images.append(image!);
            }
        }
        return images;
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    public func setupRefresh(scrollView:UIScrollView,options:ATRefreshOption,image:UIImage = UIImage(named: "icon_data_empty")!,title:String = "Data Empty..."){
        refreshEmptyData  = image
        refreshEmptyToast = title
        self.refreshData.setupRefresh(scrollView: scrollView, options: options)
    }
    public func endRefresh(more:Bool){
        self.refreshData.endRefresh(more: more)
    }
    public func endRefreshFailure(error :String = "Net Error..."){
        refreshErrorToast = error
        self.refreshData.endRefreshFailure()
    }
}
extension BaseRefreshController : ATRefreshDataSource{
    var refreshHeaderData: [UIImage] {
        return self.images
    }
    var refreshFooterData: [UIImage] {
        return self.images
    }
    var refreshLogo: UIImage{
        let newImage : UIImage = UIImage.animatedImage(with: self.images, duration: 0.35)!
        let image : UIImage = (self.refreshData.refreshing ? newImage : (self.refreshNetAvailable ? refreshEmptyData : refreshErrorData))
        return image
    }
    var refreshTitle: NSAttributedString{
        let text :String = self.refreshData.refreshing ? refreshLoaderToast : (!self.refreshNetAvailable ? refreshErrorToast : refreshEmptyToast)
        var dic : [NSAttributedString.Key : Any ] = [:]
        let font : UIFont = UIFont.systemFont(ofSize: 16)
        let color : UIColor = Appx999999
        dic.updateValue(font, forKey: .font)
        dic.updateValue(color, forKey: .foregroundColor)
        let att : NSAttributedString = NSAttributedString(string:text, attributes:(dic))
        return att
    }
    var refreshVertica: CGFloat{
        return -(NAVI_BAR_HIGHT)/2
    }
}

extension BaseRefreshController : ATRefreshDelegate{
    @objc public func refreshData(page:Int){}
}
extension BaseRefreshController{
    func playSystemSound(soundId :Int = 1118){
        AudioServicesPlaySystemSound(SystemSoundID(soundId))
    }
}
