//
//  BaseNavigationController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/29.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    private var pushing : Bool = false;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
        self.interactivePopGestureRecognizer?.isEnabled = true
        let navBar : UINavigationBar = UINavigationBar.appearance()
        navBar.titleTextAttributes = defaultNvi()
        let imageV : UIImage = UIImage.imageWithColor(color: UIColor(hex:"ffffff"))
        navBar.setBackgroundImage(imageV, for: .default)
        navBar.shadowImage = UIImage.imageWithColor(color: UIColor.white)
        navBar.isTranslucent = false
    }
    private func defaultNvi()-> [NSAttributedString.Key : Any]{
        var dic :[NSAttributedString.Key : Any] = [ : ]
        let font : UIFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
        let color : UIColor = Appx181818
        dic.updateValue(font, forKey: .font)
        dic.updateValue(color, forKey: .foregroundColor)
        return dic
    }
    override var prefersStatusBarHidden: Bool{
        return self.visibleViewController!.prefersStatusBarHidden;
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default;
    }
    override var shouldAutorotate: Bool{
        guard let visibleViewController = self.visibleViewController else { return false }
        return visibleViewController.shouldAutorotate;
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        guard let visibleViewController = self.visibleViewController else { return .portrait }
        return visibleViewController.supportedInterfaceOrientations;
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        guard let visibleViewController = self.visibleViewController else { return .portrait }
        return visibleViewController.preferredInterfaceOrientationForPresentation;
    }
}
extension BaseNavigationController :UINavigationControllerDelegate{
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.pushing == true {
            return;
        }else{
            self.pushing = true;
        }
        super.pushViewController(viewController, animated: animated);
    }
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.pushing = false;
    }
}
