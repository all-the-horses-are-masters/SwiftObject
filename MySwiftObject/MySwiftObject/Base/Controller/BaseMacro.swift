//
//  BaseMacro.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/30.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

@_exported import Hue
@_exported import ATKit_Swift
@_exported import ATRefresh_Swift
@_exported import SnapKit

let AppWindow = BaseMacro.window()
let SCREEN_WIDTH  :CGFloat  = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT :CGFloat  = UIScreen.main.bounds.size.height

let iPhone_X        : Bool      = at_iphoneX
let STATUS_BAR_HIGHT:CGFloat    = at_statusBar //状态栏
let NAVI_BAR_HIGHT  :CGFloat    = at_naviBar  //导航栏
let TAB_BAR_ADDING  :CGFloat    = at_tabBar  //iphoneX斜刘海

let AppColor     :UIColor = UIColor(hex:"007EFE")
let Appxdddddd   :UIColor = UIColor(hex:"dddddd")
let Appx000000   :UIColor = UIColor(hex:"000000")
let Appx181818   :UIColor = UIColor(hex:"181818")
let Appx333333   :UIColor = UIColor(hex:"333333")
let Appx666666   :UIColor = UIColor(hex:"666666")
let Appx999999   :UIColor = UIColor(hex:"999999")
let Appxf8f8f8   :UIColor = UIColor(hex:"f8f8f8")
let Appxffffff   :UIColor = UIColor(hex:"ffffff")
let AppRadius    :CGFloat = 3
let placeholder  :UIImage = UIImage.imageWithColor(color:UIColor(hex: "f4f4f4"))
let appDatas : [String] = ["七届传说","极品家丁","择天记","神墓","遮天"]

let AppFrame :CGRect = CGRect.init(x:20, y:STATUS_BAR_HIGHT+20, width: SCREEN_WIDTH - 40, height:CGFloat(SCREEN_HEIGHT-STATUS_BAR_HIGHT-20-20-TAB_BAR_ADDING));

class BaseMacro : NSObject{
    fileprivate class func window() -> UIWindow{
        let application  = UIApplication.shared
        guard let window = application.delegate?.window else {
            if let window = application.keyWindow {
                return window
            }
            if let window = application.windows.first {
                return window
            }
            return UIWindow(frame: UIScreen.main.bounds)
        }
        return window!
    }
    class func listView() ->Bool{
        return GKThemeTool.theme().state == .list
    }
    class func listTop() -> CGFloat{
        return listView() ? 0 : 15.0
    }
}
extension UIImage{
    class func imageWithColor(color:UIColor) -> UIImage{
         let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
         UIGraphicsBeginImageContext(rect.size)
         let context = UIGraphicsGetCurrentContext()
         context!.setFillColor(color.cgColor)
         context!.fill(rect)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         return image!
     }
}
