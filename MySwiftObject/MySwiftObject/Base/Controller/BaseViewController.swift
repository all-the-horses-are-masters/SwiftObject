//
//  BaseViewController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/29.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    deinit {
        debugPrint(self.classForCoder, "is deinit");
    }
    public var refreshNetAvailable : Bool{
        return NetworkReachabilityManager()!.isReachable
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.fd_prefersNavigationBarHidden = false
        self.fd_interactivePopDisabled = false
        self.view.backgroundColor = UIColor.white
    }
    override var prefersStatusBarHidden: Bool{
        return false;
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default;
    }
    //设置选择
    override var shouldAutorotate: Bool{
        return true
    }
    //设置是横屏 还是竖屏
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .all;
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait;
    }
}
extension BaseViewController : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true;
    }
}
