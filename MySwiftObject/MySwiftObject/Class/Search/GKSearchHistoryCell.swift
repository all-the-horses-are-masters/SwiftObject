//
//  GKSearchHistoryCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/20.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKSearchHistoryCell: UITableViewCell {
    lazy var imageV : UIImageView = {
        let imageV : UIImageView = UIImageView();
        imageV.image = UIImage.init(named: "icon_history");
        return imageV;
    }()
    lazy var titleLab : UILabel = {
        let titleLab : UILabel = UILabel();
        titleLab.font = UIFont.systemFont(ofSize: 14);
        titleLab.textColor = UIColor.init(hex: "666666");
        titleLab.textAlignment = .left;
        return titleLab
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        self.contentView.addSubview(self.imageV);
        self.contentView.addSubview(self.titleLab);
        self.imageV.snp.makeConstraints { (make) in
            make.width.height.equalTo(30);
            make.left.equalToSuperview().offset(15);
            make.centerY.equalToSuperview();
        }
        self.titleLab.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview();
            make.left.equalTo(self.imageV.snp.right).offset(10);
            make.right.equalToSuperview().offset(-15);
        }
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
