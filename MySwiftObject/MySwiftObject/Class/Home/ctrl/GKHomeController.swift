//
//  GKHomeController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKHomeController: BaseConnectionController {
    private var options : GKLoadOptions = .None
    private lazy var homeNet : GKHomeNet = {
        return GKHomeNet()
    }()
    private lazy var listData: [GKHomeInfo] = {
        return []
    }()
    private lazy var navBarView: GKHomeNavBarView = {
        let navBarView = GKHomeNavBarView.instanceView()
        navBarView.chirdenBtn.addTarget(self, action: #selector(selectAction), for: .touchUpInside)
        return navBarView
    }()
    private lazy var recyleView : KLRecycleScrollView = {
        let recyleView :KLRecycleScrollView = KLRecycleScrollView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH-120, height: 30))
        recyleView.timerEnabled = true
        recyleView.pagingEnabled = true
        recyleView.scrollInterval = 4
        recyleView.direction = KLRecycleScrollViewDirection.top
        recyleView.delegate = self;
        return recyleView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI();
        self.loadData();
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.reloadData()
    }
    private func loadUI(){
        debugPrint("当前设备是否是iPhoneX:\(iPhone_X)")
        self.options = GKLoadOptions.Default
        self.fd_prefersNavigationBarHidden = true
        self.navBarView.backgroundColor = AppColor
        self.view.addSubview(self.navBarView)
        self.navBarView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(NAVI_BAR_HIGHT)
        }
        self.collectionView.snp.remakeConstraints { (make) in
            make.top.equalTo(self.navBarView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
        self.navBarView.tapView.addSubview(self.recyleView)
        self.recyleView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.recyleView.reloadData(appDatas.count)
        self.setupRefresh(scrollView: self.collectionView, options: .defaults)
        
    }
    private func loadData(){
        GKHomeNet.reloadHomeDataNeed { [weak self](options) in
            self?.options = options;
            self?.refreshData(page: 1);
        }
    }
    override func refreshData(page: Int) {
        self.navBarView.reloadUI();
        self.homeNet.homeNet(options:self.options, sucesss: { [weak self] (object) in
            if page == RefreshPageStart{
                self?.playSystemSound()
            }
            self?.listData = object ;
            self?.collectionView.reloadData();
            self?.endRefresh(more: false);
        }) { [weak self] (error) in
            self?.endRefreshFailure();
        }
    }
    @objc private func moreAction(sender:UIButton) {
        let bookInfo :GKHomeInfo = self.listData[sender.tag];
        switch bookInfo.state {
        case .DataNet?:
            GKJump.jumpToMore(homeInfo: bookInfo)
            break
        default:
            let info = bookInfo.listData.first as Any
            if info is GKBookModel{
                GKJump.jumpToBookCase()
            }else{
                
            }
            break
        }
    }
    @objc private func selectAction(){
        GKJump.jumpToSexSelect()
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.listData.count;
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let bookInfo :GKHomeInfo = self.listData[section];
        return bookInfo.listData.count;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: listTop, bottom: 0, right: listTop);
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: 45);
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view :GKHomeReusableView = GKHomeReusableView.viewForCollectionView(collectionView: collectionView, elementKind: kind, indexPath: indexPath);
        view.moreBtn.tag = indexPath.section
        view.moreBtn.addTarget(self, action: #selector(moreAction(sender:)), for: .touchUpInside)
        let bookInfo :GKHomeInfo = self.listData[indexPath.section]
        view.titleLab.text = bookInfo.shortTitle ?? ""
        view.moreBtn.isHidden = !bookInfo.moreData
        return view;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if listView {
            return collectionView.sizeForCollectionView(classCell: GKBookTableCell.self, indexPath: indexPath, fixedValue: SCREEN_WIDTH, dynamic:.width) { (cell) in
                let bookInfo :GKHomeInfo = self.listData[indexPath.section]
                cell.model = bookInfo.listData[indexPath.row]
            }
        }
        let width = (SCREEN_WIDTH - 4*listTop)/3.0 - 1
        return collectionView.sizeForCollectionView(classCell: GKBookCell.self, indexPath: indexPath, fixedValue: width, dynamic: .width) { (cell) in
            let bookInfo :GKHomeInfo = self.listData[indexPath.section]
            cell.model = bookInfo.listData[indexPath.row]
        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if listView {
            let cell :GKBookTableCell = GKBookTableCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
            let bookInfo :GKHomeInfo = self.listData[indexPath.section]
            cell.model = bookInfo.listData[indexPath.row]
            return cell
        }
        let cell : GKBookCell = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath);
        let bookInfo :GKHomeInfo = self.listData[indexPath.section]
        cell.model = bookInfo.listData[indexPath.row]
        return cell;
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bookInfo :GKHomeInfo = self.listData[indexPath.section];
        
        let model:GKBookModel = bookInfo.listData[indexPath.row];
        switch bookInfo.state {
        case .DataNet?:
            GKJump.jumpToDetail(bookId: model.bookId!)
            break
        default:
            GKJump.jumpToNovel(bookModel: model)
            break
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent;
    }
}
extension GKHomeController :KLRecycleScrollViewDelegate{
    func recycleScrollView(_ recycleScrollView: KLRecycleScrollView!, viewForItemAt index: Int) -> UIView! {
        let label : UILabel = UILabel.init();
        label.font = UIFont.systemFont(ofSize: 14);
        label.textColor = Appx666666
        label.text = appDatas[index];
        return label;
    }
    func recycleScrollView(_ recycleScrollView: KLRecycleScrollView!, didSelect view: UIView!, forItemAt index: Int) {
        GKJump.jumpToSearch()
    }
}
