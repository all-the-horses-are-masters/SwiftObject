//
//  GKSexCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKSexCell: UICollectionViewCell {

    @IBOutlet weak var titleLab: UILabel!
    var model :GKRankModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.shortTitle ?? "";
            selectColor(select: item.select!)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectColor(select: false)
        // Initialization code
    }
    private func selectColor(select :Bool){
        self.titleLab.layer.masksToBounds = true
        self.titleLab.layer.cornerRadius = 5
        if (select){
            self.titleLab.textColor = AppColor
            self.titleLab.backgroundColor = UIColor(hex: "EBF5FF")
        }else{
            self.titleLab.textColor = Appx999999
            self.titleLab.backgroundColor = UIColor(hex: "ededed")
        }
    }

}
