//
//  GKJump.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
class GKJump: NSObject {
    class func jumpToGuideCtrl(completion:((() -> Void))? = nil){
        let user : GKUser = userInfo
        if (user.rankDatas.count == 0) {
            AppWindow.rootViewController = GKSexViewController(completion: completion);
        }else{
            if completion != nil {
                completion!();
            }
        }
    }
    class func jumpToDetail(bookId:String){
        let vc : GKDetailViewController = GKDetailViewController.vcWithBookId(bookId: bookId) ;
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    class func jumpToMore(homeInfo:GKHomeInfo){
        let vc : GKHomeMoreController = GKHomeMoreController(info:homeInfo);
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    class func jumpToClassifyTail(group:String,name:String){
        let vc:GKClassifyTailController = GKClassifyTailController(group: group, name: name)
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    class func jumpToBookCase(){
        let root : UIViewController =  UIViewController.rootTopPresentedController()
        root.tabBarController?.selectedIndex = 2
    }
    class func jumpToNovel(bookModel :GKBookModel,chapter : NSInteger = 0){
        let vc:GKNovelContentController = GKNovelContentController(bookModel: bookModel,chapter: chapter)
        vc.hidesBottomBarWhenPushed = true;
        let nvc : BaseNavigationController = BaseNavigationController(rootViewController: vc);
        nvc.modalPresentationStyle = .fullScreen;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    class func jumpToBrowse(){
        let vc:GKBrowseController = GKBrowseController();
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    class func jumpToSearch(){
        let vc:GKSearchHistoryController  = GKSearchHistoryController();
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: false);
    }
    class func jumpToSearchResult(keyWord :String){
        let vc: GKSearchResultController = GKSearchResultController(keyWord: keyWord)
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    class func jumpToSexSelect(){
        let vc = GKSexViewController();
        let nvc = BaseNavigationController(rootViewController: vc);
        nvc.modalPresentationStyle = .fullScreen;
        UIViewController.rootTopPresentedController().present(nvc, animated:false, completion: nil);
    }
    class func jumpToGridController(){
        let vc = GKGridController()
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    class func jumpToStackController(){
        let vc = GKStackViewController(nibName: "GKStackViewController_iPad", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
}
