//
//  GKChapterController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/3/31.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit

class GKChapterController: BaseTableViewController {
    convenience init(bookId :String) {
        self.init()
        self.bookId = bookId
    }
    private var bookId    : String? = ""
    private lazy var chapterDatas : [GKNovelChapter] = {
        return []
    }()
    public var bookModel : GKBookModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.tableView, options: .defaults);
    }
    override func refreshData(page: Int) {
        if self.bookId!.count > 0 {
            GKNovelNet.bookSummary(bookId: self.bookId!, sucesss: { (source : GKNovelSource) in
                GKNovelNet.bookChapters(bookId: source.sourceId!, sucesss: { (info) in
                    self.chapterDatas.append(contentsOf: info.chapters)
                    self.tableView.reloadData();
                    self.endRefresh(more:false);
                    
                }) { (error) in
                    self.endRefreshFailure()
                }
            }) { (error) in
                self.endRefreshFailure()
            }
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chapterDatas.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GKNovelDirectoryCell = GKNovelDirectoryCell.cellForTableView(tableView: tableView, indexPath: indexPath);
        cell.selectionStyle = .none;
        let model : GKNovelChapter = self.chapterDatas[indexPath.row];
        cell.imageLock.isHidden = !model.isVip;
        cell.titleLab.text = model.title;
        return cell;
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let model = self.bookModel else { return }
        GKJump.jumpToNovel(bookModel:model,chapter: indexPath.row)
    }
    override var refreshVertica: CGFloat{
        return -100
    }
}
