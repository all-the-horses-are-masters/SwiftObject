//
//  GKDetailTabView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKDetailTabView: UIView {
    @IBOutlet weak var readBtn: UIButton!
    @IBOutlet weak var leftView: UIView!
    lazy var favBtn: GKFavButton = {
        return GKFavButton(frame: CGRect(x:SCREEN_WIDTH/6-22, y:2, width: 45, height: 45), image: UIImage(named: "icon_star"))
    }()
    override func awakeFromNib() {
        self.leftView.addSubview(self.favBtn)
//        self.favBtn.setTitle("关注", for: .normal);
//        self.favBtn.setTitle("关注", for: UIControl.State(rawValue: UIControl.State.normal.rawValue|UIControl.State.highlighted.rawValue));
//
//        self.favBtn.setTitle("已关注", for: .selected);
//        self.favBtn.setTitle("已关注", for: UIControl.State(rawValue: UIControl.State.selected.rawValue|UIControl.State.highlighted.rawValue));
        self.favBtn.imageColorOn = AppColor
        self.favBtn.circleColor = AppColor
        self.favBtn.lineColor = AppColor
    }
    deinit {
        
    }
}
