//
//  GKDetailViewController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKDetailViewController: YNPageViewController,YNPageViewControllerDataSource {
    class func vcWithBookId(bookId:String) -> GKDetailViewController{
        let config = YNPageConfigration.defaultConfig();
        config?.headerViewCouldScale = true
        config?.scrollMenu = true
        config?.showNavigation = false
        config?.showTabbar = true
        config?.pageStyle = .suspensionCenter
        config?.headerViewScaleMode = .top;
        
        config?.aligmentModeCenter = true;
        config?.selectedItemColor = AppColor;
        config?.selectedItemFont = UIFont.systemFont(ofSize: 17);
        config?.itemFont = UIFont.systemFont(ofSize: 15)
        config?.menuHeight = 44;
        config?.itemMargin = 40;
        config?.lineColor = AppColor;
        config?.normalItemColor = Appx333333;
        config?.suspenOffsetY = NAVI_BAR_HIGHT;
        config?.lineWidthEqualFontWidth = true;
        let vc :GKDetailViewController = GKDetailViewController(controllers: [GKCenterController(bookId: bookId),GKChapterController(bookId:bookId)], titles: ["推荐","章节"], config:config);
        vc.delegate = vc;
        vc.dataSource = vc;
        vc.headerView = vc.topView;
        vc.bookId = bookId;
        return vc
    }
    private lazy var topView : GKDetailTopView = {
        let topView :GKDetailTopView = GKDetailTopView.instanceView()
        topView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 150 + NAVI_BAR_HIGHT)
        return topView;
    }()
    private lazy var tabView: GKDetailTabView = {
        let tab :GKDetailTabView = GKDetailTabView.instanceView()
        tab.favBtn.addTarget(self, action:#selector(favAction(sender:)), for: .touchUpInside)
        tab.readBtn.addTarget(self, action: #selector(readAction), for: .touchUpInside)
        return tab
    }()
    lazy var naviView: GKDetailNaviView = {
        let tab :GKDetailNaviView = GKDetailNaviView.instanceView()
        tab.backBtn.addTarget(self, action: #selector(goBackAction), for: .touchUpInside)
        return tab
    }()
    private var bookId:String! = nil
    private var height: CGFloat = 0;
    private var model : GKBookDetailModel?{
        didSet{
            guard let item = model else { return }
            self.topView.model = item
            self.naviView.titleLab.text = item.title
            self.controllersM.forEach { (vc) in
                if vc is GKCenterController{
                    let controller : GKCenterController = vc as! GKCenterController
                    controller.model = item
                }
                if vc is GKChapterController{
                    let controller : GKChapterController = vc as! GKChapterController
                    controller.bookModel = item
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI();
        loadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        GKCaseDataQueue.getBookModel(bookId: self.bookId) { (model) in
            guard let item = model else {
                return
            }
            self.tabView.favBtn.isSelected = item.bookId!.count > 0 ? true : false
        }
    }
    private func loadUI(){
        self.fd_prefersNavigationBarHidden = true
        self.view.addSubview(self.tabView);
        self.tabView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview();
            make.height.equalTo(49)
            make.bottom.equalToSuperview().offset(-TAB_BAR_ADDING);
        }
        self.view.addSubview(self.naviView)
        self.naviView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(NAVI_BAR_HIGHT)
        }
        self.naviView.setAlpha(value: 0)
    }
    @objc private func readAction(){
        guard let item = self.model else { return }
        guard let model = GKBookModel.deserialize(from: item.toJSONString()) else { return }
        GKJump.jumpToNovel(bookModel:model)
    }
    @objc private func favAction(sender:GKFavButton) {
        guard let item = self.model else { return  }
        guard let model = GKBookModel.deserialize(from: item.toJSONString()) else { return }
        if sender.isSelected == false {
            sender.select()
            GKCaseDataQueue.insertBookModel(bookDetail: model) { (success) in
                if success{
                    self.tabView.favBtn.isSelected = true;
                    MBProgressHUD.showMessage("收藏成功");
                }
            }
        }else{
        
            ATAlertView.showAlertView(title: "是否取消收藏？", message:"", normals:["取消"], hights:["确定"]) { (title, index) in
                if index > 0{
                    sender.deselect()
                    GKCaseDataQueue.deleteBookModel(bookId:self.bookId, sucesss: { (success) in
                        if success{
                            MBProgressHUD.showMessage("取消收藏成功");
                            self.tabView.favBtn.isSelected = false;
                        }
                    })
                }
            }
        }
    }
    @objc private func goBackAction(){
        self.goBack();
    }
    private func loadData(){
        GKClassifyNet.bookDetail(bookId: self.bookId, sucesss: { (object) in
            guard let model = GKBookDetailModel.deserialize(from: object.rawString()) else{
                return
            }
            self.model = model
        }) { (error) in

        }
//        GKClassifyNet.bookUpdate(bookId: self.bookId, sucesss: { (object) in
//            //debugPrint(object);
//        }) { (error) in
//
//        }
    }
    func pageViewController(_ pageViewController: YNPageViewController!, pageFor index: Int) -> UIScrollView! {
        let vc : UIViewController = pageViewController.controllersM![index] as! UIViewController;
        if vc is BaseTableViewController{
            let ctrl : BaseTableViewController = vc as! BaseTableViewController
            return ctrl.tableView;
        }else if vc is BaseConnectionController{
            let ctrl : BaseConnectionController = vc as! BaseConnectionController
            return ctrl.collectionView;
        }
        return UIScrollView()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return self.naviView.titleLab.isHidden ? .lightContent : .default;
    }
}
extension GKDetailViewController :YNPageViewControllerDelegate{
    func pageViewController(_ pageViewController: YNPageViewController!, contentOffsetY: CGFloat, progress: CGFloat) {
        //print(contentOffsetY,progress)
        self.naviView.setAlpha(value: progress)
        self.setNeedsStatusBarAppearanceUpdate()
    }
}
