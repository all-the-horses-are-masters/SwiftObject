//
//  GKNovelView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/10.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKNovelView: UIView {

    var contentFrame :CTFrame? = nil
    var content      :NSAttributedString?{
        didSet{
            self.setNeedsDisplay()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews();
        guard let item = self.content else { return }
        let setterRef:CTFramesetter = CTFramesetterCreateWithAttributedString(item)
        let pathRef :CGPath = CGPath(rect: self.bounds,transform: nil)
        let frameRef :CTFrame = CTFramesetterCreateFrame(setterRef, CFRangeMake(0, 0), pathRef, nil);
        self.contentFrame = frameRef
    }
    override func draw(_ rect: CGRect) {
        guard let contentFrame = self.contentFrame else { return }
        let ctx : CGContext = UIGraphicsGetCurrentContext()!
        ctx.textMatrix = .identity;
        ctx.translateBy(x: 0, y: self.bounds.height)
        ctx.scaleBy(x: 1, y: -1)
        CTFrameDraw(contentFrame, ctx);
    }

}
