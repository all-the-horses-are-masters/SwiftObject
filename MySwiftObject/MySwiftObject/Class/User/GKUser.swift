//
//  GKUserModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

public var userInfo  = GKUserManager.users()

private let GKUserInfo = "GKUserSex"
private let GKThemeInfo = "GKTheme"
public enum GKUserState : Int,HandyJSONEnum {
    case boy
    case girl
}
enum GKLookState : Int,HandyJSONEnum {
    case grid
    case list
}
public class GKUser: HandyJSON {
    var state     : GKUserState = .boy
    var rankDatas : [GKRankModel] = []
    required public init() {}
}
public class GKUserManager : NSObject{
    public class func saveUser(user :GKUser) {
        userInfo = user
        let defaults = UserDefaults.standard
        guard let dic = user.toJSON() else { return }
        defaults.set(dic, forKey: GKUserInfo)
        defaults.synchronize()
    }
    public class func users() ->GKUser{
        let data = UserDefaults.standard.object(forKey:GKUserInfo)
        let json = JSON(data as Any)
        guard let user = GKUser.deserialize(from: json.rawString()) else { return GKUser() }
        return user
    }
}

class GKTheme : HandyJSON{
    var name  :String = "网格模式"
    var state :GKLookState = .grid
    required public init() {}
}
class GKThemeTool :NSObject{
    static let manager = GKThemeTool()
    var theme :GKTheme?{
        get{
            return GKThemeTool.theme()
        }
    }
    class func setState(state :GKLookState){
        let theme : GKTheme = GKThemeTool.manager.theme!
        if theme.state != state{
            theme.state = state
            theme.name = (state == .grid) ? "网格模式" : "列表模式"
            GKThemeTool.saveTheme(theme: theme)
        }
    }
    class func saveTheme(theme :GKTheme){
        let defaults = UserDefaults.standard
        guard let dic = theme.toJSON() else { return }
        defaults.set(dic, forKey: GKThemeInfo)
        defaults.synchronize()
    }
    class func theme() ->GKTheme{
        let data = UserDefaults.standard.object(forKey: GKThemeInfo)
        let json = JSON(data as Any)
        
        guard let theme = GKTheme.deserialize(from: json.rawString()) else { return GKTheme() }
        return theme
    }
}

public class ATUser: HandyJSON {
    var userId     : String = ""
    var nickName   : String = ""
    var name       : String = ""
    var age        : Int    = 0
    required public init() {}
}

