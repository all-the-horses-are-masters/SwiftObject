//
//  GKBookCaseController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBookCaseController: BaseConnectionController {
    private lazy var listData: [GKBookModel] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "我的书架")
        self.setupRefresh(scrollView: self.collectionView, options: .defaults)
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        self.refreshData(page: 1)
    }
    override func refreshData(page: Int) {
        GKCaseDataQueue.getBookModels(page: page, size: RefreshPageSize) { (datas) in
            if page == RefreshPageStart{
                self.listData.removeAll()
            }
            self.listData.append(contentsOf:datas)
            self.collectionView.reloadData()
            if self.listData.count == 0{
                self.endRefreshFailure()
            }else{
                self.endRefresh(more: datas.count >= RefreshPageSize);
            }
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let top :CGFloat = listTop
        return UIEdgeInsets(top: top, left: top, bottom: top, right: top)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if listView {
            return collectionView.sizeForCollectionView(classCell: GKBookTableCell.self, indexPath: indexPath, fixedValue: SCREEN_WIDTH, dynamic:.width) { (cell) in
                cell.model = self.listData[indexPath.row]
            }
        }
        let width = (SCREEN_WIDTH - 60)/3.0 - 1
        return collectionView.sizeForCollectionView(classCell: GKBookCell.self, indexPath: indexPath, fixedValue: width, dynamic:.width) { (cell) in
            cell.model = self.listData[indexPath.row]
        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if listView {
            let cell :GKBookTableCell = GKBookTableCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
            cell.model = self.listData[indexPath.row]
            return cell
        }
        let cell : GKBookCell = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath);
        cell.model = self.listData[indexPath.row]
        return cell;
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model:GKBookModel = self.listData[indexPath.row]
        GKJump.jumpToNovel(bookModel: model)
    }
}
