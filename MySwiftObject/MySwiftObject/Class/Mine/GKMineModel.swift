//
//  GKMineModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKMineModel: BaseModel {
    var title:String    = "";
    var icon:String     = "";
    var subTitle:String = "";
    convenience init(title:String,icon:String,subTitle:String) {
        self.init()
        self.title = title;
        self.subTitle = subTitle;
        self.icon = icon;
    }

}
