//
//  GKCaseDataQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/9.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit


private let tableName = "bookCase";

class GKCaseDataQueue: NSObject {
    class func insertBookModel(bookDetail:GKBookModel,sucesss:@escaping ((_ success : Bool) ->Void)){
        bookDetail.updateTime = GKNumber.timeStamp()
        BaseQueue.insertData(tableName: tableName, primaryId: bookDetail.bookId!, content: bookDetail.toJSONString()!) { (res) in
            if res{
                GKHomeNet.reloadHomeData(options: GKLoadOptions.Database)
            }
            sucesss(res)
        }
    }
    class func deleteBookModel(bookId:String,sucesss:@escaping ((_ success : Bool) ->Void)){
        BaseQueue.deleteData(tableName: tableName, primaryId: bookId) { (res) in
            if res{
                GKHomeNet.reloadHomeData(options: GKLoadOptions.Database)
            }
            sucesss(res)
        }
    }
    class func getBookModel(bookId:String,sucesss:@escaping ((_ bookModel : GKBookModel? ) ->Void)){
        BaseQueue.searchData(tableName: tableName, primaryId:bookId) { (json) in
            guard let data = [GKBookModel].deserialize(from: json.rawString())else{
                sucesss(nil)
                return
            }
            sucesss(data.first as? GKBookModel)
        }
    }
    class func getBookModels(page :Int,size: Int,sucesss:@escaping ((_ listData : [GKBookModel]) ->Void)){
        BaseQueue.searchData(tableName: tableName, page: page, size: size) { (json) in
            var arrayDatas :[GKBookModel] = []
            guard let data = [GKBookModel].deserialize(from: json.rawString())else{
                sucesss([])
                return
            }
            arrayDatas = data as! [GKBookModel]
            sucesss(arrayDatas)
        }
    }
    class func getBookModels(sucesss:@escaping ((_ listData :[GKBookModel]) ->Void)){
        BaseQueue.searchData(tableName: tableName) { (json) in
            var arrayDatas :[GKBookModel] = []
            guard let data = [GKBookModel].deserialize(from: json.rawString())else{
                sucesss([])
                return
            }
            arrayDatas = data as! [GKBookModel];
            sucesss(GKCaseDataQueue.sortDatas(listDatas: arrayDatas, aspeing: false))
        }
    }
    ////yes 升序
    class func sortDatas(listDatas:[GKBookModel],aspeing : Bool) ->[GKBookModel]{
        var datas  = listDatas
        datas.sort(by: {Double($0.updateTime) < Double($1.updateTime)})
//        datas.sort { (model1, model2) -> Bool in
//            return (Double(model1.updateTime) < Double(model2.updateTime))  ? aspeing : !aspeing;
//        }
        return datas
    }
}
