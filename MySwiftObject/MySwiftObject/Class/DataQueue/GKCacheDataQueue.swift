//
//  GKBookCacheDataQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKCacheDataQueue: NSObject {
    class func insertContent(bookId:String, content :GKNovelContent,completion:@escaping ((_ success : Bool) -> Void)){
        BaseQueue.insertData(tableName: bookId, primaryId: content.bookId , content: content.toJSONString() ?? "", completion: completion)
    }
    class func getContent(bookId:String, chapterId:String,completion:@escaping ((_ content : GKNovelContent?) -> Void)){
        BaseQueue.searchData(tableName: bookId, primaryId: chapterId) { (json) in
            guard let data = [GKNovelContent].deserialize(from: json.rawString())else {
                completion(nil)
                return
            }
            completion(data.first as? GKNovelContent)
        }
    }
    class func dropTable(bookId:String,completion:@escaping ((_ success : Bool) ->Void)){
        BaseQueue.dropTable(bookId,completion:completion)
    }
}
