//
//  GKBrowseDataQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

private let tableName = "bookBrowse";

class GKBrowseDataQueue: NSObject {
    public class func insertBookModel(browse:GKBrowseModel,completion: @escaping ((_ success : Bool) ->Void)){
        browse.updateTime = GKNumber.timeStamp();
        guard let bookId = browse.bookId else { return }
        BrowseQueue.insertData(primaryId: bookId, content: browse.toJSONString() ?? "", chap: browse.chapter!, page: browse.pageIndex!, time: browse.updateTime, completion: completion)
        //browse.toJSONString() ?? "" 性能上有点小问题
    }
    public class func update(bookId :String,content : String?,chap :Int,page :Int,time :TimeInterval,completion:@escaping ((_ success : Bool) -> Void)){
        BrowseQueue.updateData(primaryId: bookId, content: content, chap: chap, page: page, time: time, completion: completion)
    }
    public class func deleteBookModel(bookId:String,completion:@escaping ((_ success : Bool) ->())){
        BrowseQueue.deleteData(primaryId: bookId, completion: completion)
    }
    public class func getBookModel(bookId:String,needChapter :Bool,completion:@escaping ((_ bookModel : GKBrowseModel?) ->Void)){
        BrowseQueue.searchData(primaryId: bookId,needChapter :needChapter, completion: completion)
    }
    class func getBookModel(page:NSInteger,size:NSInteger,completion:@escaping ((_ listData : [GKBrowseModel]) ->Void)){
        BrowseQueue.searchData(page: page, size: size, completion: completion)
    }
}
