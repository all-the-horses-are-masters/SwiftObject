//
//  GKClassifyTailController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKClassifyTailController: BaseConnectionController {
    convenience init(group:String,name:String) {
        self.init();
        self.group = group
        self.name = name
    }
    private var group :String!
    private var name :String!
    
    public var rankId :String?{
        didSet{
            self.refreshData(page: 1)
        }
    }
    private lazy var listData: [GKBookModel] = {
        return []
    }()
    
    override func vtm_prepareForReuse() {
        if !self.refreshNetAvailable {
            self.listData.removeAll();
            self.collectionView.reloadData();
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: self.name)
        self.setupRefresh(scrollView: self.collectionView, options:.defaults);
    }
    override func refreshData(page: Int) {
        guard let rankId = self.rankId else {
            loadClassifyData(page: page)
            return
        }
        loadMoreData(page: page,rankId: rankId)
        
    }
    private func loadClassifyData(page: Int){
        GKClassifyNet.classifyTail(group: self.group, name: self.name, page: page, sucesss: { (respond) in
            if page == RefreshPageStart{
                self.listData.removeAll();
            }
            if let list : [JSON] = respond["books"].array{
                for obj in list{
                    if let model : GKBookModel = GKBookModel.deserialize(from: obj.rawString()){
                        self.listData.append(model);
                    }
                }
                self.collectionView.reloadData();
                self.endRefresh(more: list.count > 0 ? true : false)
            }
        }) { (error) in
            self.endRefreshFailure();
        }
    }
    private func loadMoreData(page: Int,rankId :String) {
        GKHomeNet.homeHot(rankId:rankId, sucesss: { (object) in
            if page == RefreshPageStart{
                self.listData.removeAll()
            }
            guard let info =  GKHomeInfo.deserialize(from: object["ranking"].rawString())else {
                self.endRefreshFailure()
                return
            }
            self.listData.append(contentsOf: info.books)
            self.collectionView.reloadData()
            self.endRefresh(more: false)
        }) { (error) in
            self.endRefreshFailure()
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return listTop
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let top :CGFloat = listTop
        return UIEdgeInsets(top: top, left: top, bottom: top, right: top)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if listView {
//            return collectionView.sizeForCollectionView(classCell: GKBookTableCell.self, indexPath: indexPath, fixedValue: SCREEN_WIDTH - 1, dynamic:.width) { (cell) in
//                cell.model = self.listData[indexPath.row]
//            }
            return CGSize(width: SCREEN_WIDTH, height: 145)
        }
        let width = (SCREEN_WIDTH - 60)/3.0 - 1
        let height = width * 1.35 + 60
        return CGSize(width: width, height: height)
//        return collectionView.sizeForCollectionView(classCell: GKBookCell.self, indexPath: indexPath, fixedValue: width, dynamic:.width) { (cell) in
//            cell.model = self.listData[indexPath.row]
//        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if listView {
            let cell :GKBookTableCell = GKBookTableCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
            cell.model = self.listData[indexPath.row]
            return cell
        }
        let cell : GKBookCell = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath);
        cell.model = self.listData[indexPath.row]
        return cell;
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model:GKBookModel = self.listData[indexPath.row]
        GKJump.jumpToDetail(bookId: model.bookId ?? "")
       // GKJump.jumpToNovel(bookModel: model)
    }

}
