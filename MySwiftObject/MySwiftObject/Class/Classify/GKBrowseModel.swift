//
//  GKBrowseModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBrowseModel: HandyJSON {
    
    var bookId     :String?       = ""
    var updateTime :TimeInterval  = 0
    var chapter    :NSInteger?    = 0
    var pageIndex  :NSInteger?    = 0
    
    var bookModel  :GKBookModel?        = nil
    var source     :GKNovelSource?      = nil
    var chapterInfo:GKNovelChapterInfo? = nil
    required init() {}
}
