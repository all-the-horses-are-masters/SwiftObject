//
//  GKClassifyItemController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit


class GKClassifyItemController: BaseConnectionController {
    var titleName : String?{
        didSet{
            self.refreshData(page: 1)
        }
    }
    private lazy var listData: [GKClassifyModel] = {
        return [];
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.collectionView, options:ATRefreshOption(rawValue: ATRefreshOption.autoHeader.rawValue | ATRefreshOption.header.rawValue))
    }
    override func vtm_prepareForReuse() {
        if !self.refreshNetAvailable {
            self.listData.removeAll();
            self.collectionView.reloadData();
        }
    }
    override func refreshData(page: Int) {
        guard let titleName = self.titleName else { return  }
        GKClassifyNet.classify(category:titleName, sucesss: { (object) in
            if page == RefreshPageStart{
                self.listData.removeAll();
                if self == self.magic.currentViewController(){
                    self.playSystemSound()
                }
            }
            if let list : [JSON] = object[self.titleName!].array{
                for obj in list{
                    guard let user = try?  JSONDecoder().decode(GKClassifyModel.self, from: obj.rawData()) else { return }
                    self.listData.append(user)
                }
                self.collectionView.reloadData()
                self .endRefresh(more:false)
            }
            
        }) { (error) in
            self.endRefreshFailure()
        }
        
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15);
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (SCREEN_WIDTH - 60)/3.0 - 1;
        return collectionView.sizeForCollectionView(classCell: GKClassifyCell.self, indexPath: indexPath, fixedValue: width, dynamic: .width) { (cell) in
            cell.model = self.listData[indexPath.row];
        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :GKClassifyCell = GKClassifyCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath);
        cell.model = self.listData[indexPath.row];
        return cell;
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model:GKClassifyModel = self.listData[indexPath.row];
        GKJump.jumpToClassifyTail(group: self.titleName!, name: model.title!)
    }
    
}
