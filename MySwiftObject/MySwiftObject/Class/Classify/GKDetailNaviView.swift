//
//  GKDetailNaviView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/12/10.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit

class GKDetailNaviView: UIView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor .clear
        self.backBtn.setImage(UIImage(named: "icon_detai_back_1"), for: .normal)
        self.backBtn.setImage(UIImage(named: "icon_detai_back_1"), for: UIControl.State.init(rawValue:UIControl.State.normal.rawValue|UIControl.State.highlighted.rawValue))
        
        self.backBtn.setImage(UIImage(named: "icon_detai_back_2"), for: .selected)
        self.backBtn.setImage(UIImage(named: "icon_detai_back_2"), for: UIControl.State.init(rawValue:UIControl.State.selected.rawValue|UIControl.State.highlighted.rawValue))
        self.setAlpha(value: 0)
    }
    func setAlpha(value :CGFloat){
        self.mainView.alpha = value
        self.titleLab.isHidden = value == 1 ? false : true
        self.backBtn.isSelected = value > 0.5
    }
}
