//
//  GKBookModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBookModel:HandyJSON {
    var bookId    :String?       = "";
    var updateTime:TimeInterval  = 0;
    var author    :String? = "";
    var cover     :String? = "";
    var shortIntro:String? = ""
    var title     :String? = "";
    var majorCate :String? = "";
    var minorCate :String? = "";
    var lastChapter:String? = "";
    
    var retentionRatio :Float? = 0.0;
    var latelyFollower :Int?   = 0;
    required init() {}
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.bookId     <-- ["bookId","_id"]
        mapper <<< self.shortIntro <-- ["shortIntro","longIntro"]
    }
  
}

class GKBookDetailModel: GKBookModel {
    var cat          :String? = ""
    var chaptersCount:String? = ""
    var creater      :String? = ""
    
    var postCount    :Int? = 0
    var wordCount    :Int? = 0
    
    var hasSticky    :Bool? = false
    var hasUpdated   :Bool? = false
    var hasCp        :Bool? = false
    var isSerial     :Bool? = false
    var donate       :Bool? = false
    var le           :Bool? = false
    var allowVoucher :Bool? = false
    
    var numberLine   :Int = 3
}


class GKBookUpdateInfo : GKBookDetailModel{
    var tags         :[String] = []
    var copyrightInfo:String   = ""
    var updated      :String   = ""
}
