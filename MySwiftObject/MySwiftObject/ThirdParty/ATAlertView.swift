//
//  ATAlertView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/3/21.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
class ATAlertView: NSObject {
    class func showAlertView(title:String? = nil,message:String? = nil,normals:NSArray? = nil,hights:NSArray? = nil,completion: @escaping ((_ title:String,_ index :NSInteger) -> Void)){
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert);
        if normals != nil{
            for (index,object) in normals!.enumerated() {
                let action = UIAlertAction(title:(object as! String), style: .cancel) { (alert) in
                    completion(object as! String,index);
                };
                alertView.addAction(action);
            }
        }
        if hights != nil {
            for (index,sure) in hights!.enumerated() {
                let action = UIAlertAction(title:(sure as! String), style: .destructive) { (alert) in
                    completion(sure as! String,index + (normals != nil ? normals!.count : 0));
                };
                alertView.addAction(action);
            }
        }
        let rootVC = UIViewController.rootTopPresentedController();
        rootVC.present(alertView, animated:true, completion: nil);
    }
}

class ATActionSheet: NSObject {
    class func showActionSheet(title:String? = nil,message:String? = nil,normals:NSArray? = nil,hights:NSArray? = nil,completion:@escaping ((_ title:String,_ index:NSInteger)->Void)){
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet);
        let rootVC = UIViewController.rootTopPresentedController();
        if normals != nil{
            for (index, object) in normals!.enumerated() {
                let action = UIAlertAction(title: (object as! String), style:.default) { (alert) in
                    completion(object as! String,index);
                }
                actionSheet.addAction(action);
            }
        }
        if hights != nil {
            for (index, object) in hights!.enumerated() {
                let action = UIAlertAction(title: (object as? String), style:.destructive) { (alert) in
                    completion(object as! String,index + (normals != nil ? normals!.count : 0));
                }
                actionSheet.addAction(action);
            }
        }
        rootVC.present(actionSheet, animated:true, completion: nil);
    }
}
